# PHP Arch Linux repository

Unofficial auto-updated repository for personal use. Feel free to use/reuse/publish.

```
[archlinux-php]
SigLevel = Never
Server = https://archlinux-php.gitlab.io/repository
```

All credits goes to [archlinux-aur](https://gitlab.com/archlinux-aur/support/wikis/how-it-works).